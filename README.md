# product-api

RESTful api in Spring for consuming Products

The service is run the following maven command:

mvn spring-boot:run -Ddebug.port=5005

This will launch the application server and expose the REST endpoints, opening a debug port on 5005. 

The REST api exposes 5 endpoints, as follows:

1.

URL: http://localhost:8080/packages
Description: retrieves all packages in the database
Method: GET

2.

URL: http://localhost:8080/packages/create
Method: POST
Description: creates a new package in the database, with specified name and description

3. 

URL: http://localhost:8080/package/{id}?currency={currency}
Method: GET
Description: retrieves the existing package with id {id}, also accepts the optional parameter currency {currency} to transform the price of the result into that currency 

4.

URL: http://localhost:8080/packages/update
Method: POST
Description: updates an existing package in the database, matching the specified id, with specified name and description

5.

URL: http://localhost:8080/package/{id}
Method: DELETE
Description: deletes an existing package in the database
