package code.lewis.controller;

import code.lewis.model.Package;
import code.lewis.request.CreatePackage;
import code.lewis.request.UpdatePackage;
import code.lewis.service.PackageCreator;
import code.lewis.service.PackageDeleter;
import code.lewis.service.PackageRetriever;
import code.lewis.service.PackageUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class PackageController {

    @Autowired
    private PackageCreator creator;

    @Autowired
    private PackageRetriever retriever;

    @Autowired
    private PackageUpdater updater;

    @Autowired
    private PackageDeleter deleter;

    @RequestMapping(method = RequestMethod.GET, value = "/packages", produces = "application/json")
    public ResponseEntity<List<Package>> packages() {
        return ResponseEntity.ok(retriever.retrieve());
    }

    @RequestMapping(method = RequestMethod.POST, value = "packages/create", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Package> create(@RequestBody CreatePackage createPackage) {
        return ResponseEntity.ok(creator.create(createPackage.getName(), createPackage.getDescription()));
    }

    @RequestMapping(method = RequestMethod.GET, value = "package/{id}", produces = "application/json")
    public ResponseEntity<Package> retrieve(@PathVariable(value = "id", required = true) UUID id, @RequestParam(value = "currency", required = false) String currency) {

        Package pack = retriever.retrieve(id, currency);

        if(pack == null) return ResponseEntity.notFound().build();

        return ResponseEntity.ok(pack);
    }

    @RequestMapping(method = RequestMethod.POST, value = "packages/update", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Boolean> update(@RequestBody UpdatePackage updatePackage) {
        return ResponseEntity.ok(updater.update(updatePackage.getId(), updatePackage.getName(), updatePackage.getDescription()));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "package/{id}", produces = "application/json")
    public ResponseEntity<Boolean> delete(@PathVariable(value = "id", required = true) UUID id) {
        return ResponseEntity.ok(deleter.delete(id));
    }
}