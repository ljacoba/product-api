package code.lewis.service;

import code.lewis.database.PackagePropertiesDatabase;
import code.lewis.exception.IntervalServerErrorException;
import code.lewis.model.Package;
import code.lewis.model.PackageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.UUID;

@Service
public class PackageCreator {

    @Autowired
    private PackagePropertiesDatabase packagePropertiesDatabase;
    
    public Package create(String name, String description) {

        PackageProperties packageProperties = new PackageProperties(
                UUID.randomUUID(),
                name,
                description);

        boolean success = packagePropertiesDatabase.create(packageProperties);

        if(! success) {
            throw new IntervalServerErrorException();
        }

        return new Package(
                packageProperties.getId(),
                packageProperties.getName(),
                packageProperties.getDescription(),
                Collections.emptyList(),
                0);
    }
}
