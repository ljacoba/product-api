package code.lewis.service;

import code.lewis.model.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PriceCalculator {

    public double calculate(List<Product> products, double exchangeRate) {
        return products.stream().map(product -> product.getUsdPrice() * exchangeRate).mapToDouble(Double::doubleValue).sum();
    }
}
