package code.lewis.service;

import code.lewis.database.CurrencyDatabase;
import code.lewis.database.PackagePropertiesDatabase;
import code.lewis.database.ProductDatabase;
import code.lewis.exception.CurrencyDoesNotExistException;
import code.lewis.exception.PackageDoesNotExistException;
import code.lewis.model.Package;
import code.lewis.model.PackageProperties;
import code.lewis.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PackageRetriever {

    @Autowired
    private PackagePropertiesDatabase packagePropertiesDatabase;

    @Autowired
    private ProductDatabase productDatabase;

    @Autowired
    private CurrencyDatabase currencyDatabase;

    @Autowired
    private PriceCalculator priceCalculator;

    public List<Package> retrieve() {

        List<PackageProperties> packageProperties = packagePropertiesDatabase.retrieve();

        return packageProperties.stream().map(properties -> {

            List<Product> products = productDatabase.retrieveProducts(properties.getId());

            return new Package(
                    properties.getId(),
                    properties.getName(),
                    properties.getDescription(),
                    products,
                    priceCalculator.calculate(products, 1));

        }).collect(Collectors.toList());
    }

    public Package retrieve(UUID id, String currency) {

        PackageProperties packageProperties = packagePropertiesDatabase.retrieve(id);

        if(packageProperties == null) {
            throw new PackageDoesNotExistException();
        }

        Double exchangeRate = currencyDatabase.exchangeRate(currency);

        if (exchangeRate == null) {
            throw new CurrencyDoesNotExistException();
        }

        List<Product> products = productDatabase.retrieveProducts(packageProperties.getId());

        return new Package(
                packageProperties.getId(),
                packageProperties.getName(),
                packageProperties.getDescription(),
                productDatabase.retrieveProducts(packageProperties.getId()),
                priceCalculator.calculate(products, exchangeRate));
    }
}
