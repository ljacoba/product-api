package code.lewis.service;

import code.lewis.database.PackagePropertiesDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class PackageDeleter {

    @Autowired
    private PackagePropertiesDatabase packagePropertiesDatabase;
    
    public boolean delete(UUID id) {
        return packagePropertiesDatabase.delete(id);
    }
}
