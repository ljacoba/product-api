package code.lewis.service;

import code.lewis.database.PackagePropertiesDatabase;
import code.lewis.model.PackageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class PackageUpdater {

    @Autowired
    private PackagePropertiesDatabase packagePropertiesDatabase;
    
    public boolean update(UUID id, String name, String description) {

        PackageProperties packageProperties = new PackageProperties(
                id,
                name,
                description);

        return packagePropertiesDatabase.update(packageProperties);
    }
}
