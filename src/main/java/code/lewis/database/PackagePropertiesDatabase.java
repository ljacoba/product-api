package code.lewis.database;

import code.lewis.exception.PackageDoesNotExistException;
import code.lewis.model.PackageProperties;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PackagePropertiesDatabase {

    private final Map<UUID, PackageProperties> packageProperties;

    public PackagePropertiesDatabase() {
        this.packageProperties = new HashMap<>();
    }

    public boolean create(PackageProperties pack) {

        PackageProperties storedPackageProperties = packageProperties.get(pack.getId());

        if(storedPackageProperties != null) {

            if(storedPackageProperties.equals(pack)) {

                // Package properties are the same, so there is no need to return failure assuming this is using PUT behaviour

                return true;
            }

            throw new PackageDoesNotExistException();
        }

        packageProperties.put(pack.getId(), pack);

        return true;
    }

    public PackageProperties retrieve(UUID id) {

        // don't throw package does not exist exception here, as it should be possible to check the absence of a packageProperties in this way

        return packageProperties.get(id);
    }

    public List<PackageProperties> retrieve() {
        return new ArrayList<>(packageProperties.values());
    }

    public boolean update(PackageProperties pack) {

        PackageProperties storedPackageProperties = packageProperties.get(pack.getId());

        if(storedPackageProperties == null) {
            throw new PackageDoesNotExistException();
        }

        if(storedPackageProperties.equals(pack)) {

            // Package properties are the same, so there is no need to do anything

            return true;
        }

        packageProperties.put(pack.getId(), pack);

        return true;
    }

    public boolean delete(UUID id) {

        PackageProperties storedPackageProperties = packageProperties.get(id);

        if(storedPackageProperties == null) {
            throw new PackageDoesNotExistException();
        }

        packageProperties.remove(id);

        return true;
    }
}
