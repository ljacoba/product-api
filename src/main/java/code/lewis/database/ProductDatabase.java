package code.lewis.database;

import code.lewis.exception.ProductDatabaseUnavailableException;
import code.lewis.model.Product;
import org.springframework.http.*;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class ProductDatabase {

    private static final String PRODUCTS_URL = "https://product-service.herokuapp.com/api/v1/products";

    // These credentials could also passed through application configuration to mask them from the codebase
    private static final String AUTH_USER = "user";
    private static final String AUTH_PASSWORD = "pass";

    public List<Product> retrieveProducts(UUID packageId) {

        RestTemplate restTemplate = new RestTemplate();

        restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(AUTH_USER, AUTH_PASSWORD));

        ResponseEntity<Product[]> response = restTemplate.getForEntity(PRODUCTS_URL, Product[].class);

        if(response.getStatusCode() != HttpStatus.OK) {
            throw new ProductDatabaseUnavailableException();
        }

        return Arrays.asList(response.getBody());
    }
}
