package code.lewis.database;

import code.lewis.exception.CurrencyDatabaseUnavailableException;
import code.lewis.exception.CurrencyDoesNotExistException;
import code.lewis.model.Currencies;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CurrencyDatabase {

    // Uses EUR as a base, so translation required for USD
    private static final String CURRENCY_URL = "http://data.fixer.io/api/latest?access_key=4ae793b1fa2d8f8daec25404913e4179";

    private static final String BASE_CURRENCY = "USD";

    /**
     *
     * @param currency Currency to retrieve the exchange rate for, in relation to USD
     * @return the conversion rate between USD and the specified currency
     */
    public Double exchangeRate(String currency) {

        if(currency == null || currency.isEmpty()) currency = BASE_CURRENCY;

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Currencies> response = restTemplate.getForEntity(CURRENCY_URL, Currencies.class);

        if(response.getStatusCode() != HttpStatus.OK) {
            throw new CurrencyDatabaseUnavailableException();
        }

        Double exchangeRate = response.getBody().getRates().get(currency);

        if(exchangeRate == null) {
            throw new CurrencyDoesNotExistException();
        }

        Double usdExchangeRate = response.getBody().getRates().get(BASE_CURRENCY);

        if(usdExchangeRate == null) {
            throw new CurrencyDoesNotExistException();
        }

        return exchangeRate * (1 / usdExchangeRate);
    }
}
