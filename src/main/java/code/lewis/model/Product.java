package code.lewis.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Product {

    private final String id;
    private final String name;
    private final long usdPrice;

    @JsonCreator
    public Product(@JsonProperty(value = "id", required = true) final String id,
                   @JsonProperty(value = "name", required = true) final String name,
                   @JsonProperty(value = "usdPrice", required = true) final long usdPrice) {

        this.id = id;
        this.name = name;
        this.usdPrice = usdPrice;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getUsdPrice() {
        return usdPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return usdPrice == product.usdPrice &&
                Objects.equals(id, product.id) &&
                Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, usdPrice);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", usdPrice=" + usdPrice +
                '}';
    }
}
