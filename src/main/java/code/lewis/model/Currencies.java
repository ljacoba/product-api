package code.lewis.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class Currencies {

    private final boolean success;
    private final long timestamp;
    private final String base;
    private final Date date;
    private final Map<String, Double> rates;

    @JsonCreator
    public Currencies(@JsonProperty(value = "success", required = true) final boolean success,
                      @JsonProperty(value = "timestamp", required = true) final long timestamp,
                      @JsonProperty(value = "base", required = true) final String base,
                      @JsonProperty(value = "date", required = true) final Date date,
                      @JsonProperty(value = "rates", required = true) final Map<String, Double> rates) {

        this.success = success;
        this.timestamp = timestamp;
        this.base = base;
        this.date = date;
        this.rates = rates;
    }

    public boolean isSuccess() {
        return success;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getBase() {
        return base;
    }

    public Date getDate() {
        return date;
    }

    public Map<String, Double> getRates() {
        return rates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currencies that = (Currencies) o;
        return success == that.success &&
                timestamp == that.timestamp &&
                Objects.equals(base, that.base) &&
                Objects.equals(date, that.date) &&
                Objects.equals(rates, that.rates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(success, timestamp, base, date, rates);
    }

    @Override
    public String toString() {
        return "Currencies{" +
                "success=" + success +
                ", timestamp=" + timestamp +
                ", base='" + base + '\'' +
                ", date=" + date +
                ", rates=" + rates +
                '}';
    }
}
