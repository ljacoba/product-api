package code.lewis.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.UUID;

public class PackageProperties {

    private UUID id;
    private String name;
    private String description;

    @JsonCreator
    public PackageProperties(@JsonProperty(value = "id", required = true) final UUID id,
                             @JsonProperty(value = "name", required = true) final String name,
                             @JsonProperty(value = "description", required = true) final String description) {

        this.id = id;
        this.name = name;
        this.description = description;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PackageProperties aPackage = (PackageProperties) o;
        return Objects.equals(id, aPackage.id) &&
                Objects.equals(name, aPackage.name) &&
                Objects.equals(description, aPackage.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }

    @Override
    public String toString() {
        return "Package{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
