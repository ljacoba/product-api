package code.lewis.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Package {

    private final UUID id;
    private final String name;
    private final String description;
    private final List<Product> products;
    private final double price;

    @JsonCreator
    public Package(@JsonProperty(value = "id", required = true) final UUID id,
                   @JsonProperty(value = "name", required = true) final String name,
                   @JsonProperty(value = "description", required = true) final String description,
                   @JsonProperty(value = "products", required = true) final List<Product> products,
                   @JsonProperty(value = "price", required = true) final double price) {

        this.id = id;
        this.name = name;
        this.description = description;
        this.products = products;
        this.price = price;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<Product> getProducts() {
        return products;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Package aPackage = (Package) o;
        return price == aPackage.price &&
                Objects.equals(id, aPackage.id) &&
                Objects.equals(name, aPackage.name) &&
                Objects.equals(description, aPackage.description) &&
                Objects.equals(products, aPackage.products);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, products, price);
    }

    @Override
    public String toString() {
        return "Package{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", products=" + products +
                ", price=" + price +
                '}';
    }
}
