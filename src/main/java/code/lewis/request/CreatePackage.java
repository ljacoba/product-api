package code.lewis.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class CreatePackage {

    private final String name;
    private final String description;

    @JsonCreator
    public CreatePackage(@JsonProperty(value = "name", required = true) String name,
                         @JsonProperty(value = "description", required = true) String description) {

        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreatePackage that = (CreatePackage) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description);
    }

    @Override
    public String toString() {
        return "CreatePackage{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
