package code.lewis.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
public class ProductDatabaseUnavailableException extends RuntimeException{

    private static final String MESSAGE = "Product Database unavailable, please try again later or contact a system administrator.";

    public ProductDatabaseUnavailableException() {
        super(MESSAGE);
    }
}
