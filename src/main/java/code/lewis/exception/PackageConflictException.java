package code.lewis.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class PackageConflictException extends RuntimeException{

    private static final String MESSAGE = "There was a conflict with an existing package.";

    public PackageConflictException() {
        super(MESSAGE);
    }
}
