package code.lewis.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
public class CurrencyDatabaseUnavailableException extends RuntimeException{

    private static final String MESSAGE = "Currency Database unavailable, please try again later or contact a system administrator.";

    public CurrencyDatabaseUnavailableException() {
        super(MESSAGE);
    }
}
