package code.lewis.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PackageDoesNotExistException extends RuntimeException{

    private static final String MESSAGE = "The package you are interacting with does not exist.";

    public PackageDoesNotExistException() {
        super(MESSAGE);
    }
}
