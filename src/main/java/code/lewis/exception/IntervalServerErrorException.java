package code.lewis.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class IntervalServerErrorException extends RuntimeException {

    private static final String MESSAGE = "Sorry, we are unable to complete your request, please try again later or contact a system administrator.";

    public IntervalServerErrorException() {
        super(MESSAGE);
    }
}
